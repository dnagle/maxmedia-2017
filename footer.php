<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package maxmedia_2017
 */

?>

	</div><!-- #content -->
<?php
	// $my_page_id value is captured at the top of the page
	global $my_page_id;

	$my_theme_slug = get_option('stylesheet');
	$my_theme_mods = get_option("theme_mods_$my_theme_slug");
	$my_contactpage_id = $my_theme_mods['maxmedia_contactpage_id'];

	if ( $my_contactpage_id && $my_page_id == $my_contactpage_id ) :
?>

	<div id="contact-office-location">
	<?php echo maxmedia_2017_hexagon_heading_shortcode( array( 'image' => 'two' ), 'Office location'); ?>
	<div class="map-banner" role="complementary">
		<div id="office-map" class="map-frame"></div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=<?php echo rfc3986UrlEncode(trim( get_theme_mod('hioice_contact_gmaps_api_key') )); ?>"></script>
<script>
var map;
var MAP_PIN="M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z";
var inherits=function(childCtor,parentCtor){function tempCtor(){}tempCtor.prototype=parentCtor.prototype;childCtor.superClass_=parentCtor.prototype;childCtor.prototype=new tempCtor;childCtor.prototype.constructor=childCtor};function Marker(options){google.maps.Marker.apply(this,arguments);if(options.map_icon_label){this.MarkerLabel=new MarkerLabel({map:this.map,marker:this,text:options.map_icon_label});this.MarkerLabel.bindTo("position",this,"position")}}inherits(Marker,google.maps.Marker);Marker.prototype.setMap=function(){google.maps.Marker.prototype.setMap.apply(this,arguments);this.MarkerLabel&&this.MarkerLabel.setMap.apply(this.MarkerLabel,arguments)};var MarkerLabel=function(options){var self=this;this.setValues(options);this.div=document.createElement("div");this.div.className="map-icon-label";google.maps.event.addDomListener(this.div,"click",function(e){e.stopPropagation&&e.stopPropagation();google.maps.event.trigger(self.marker,"click")})};MarkerLabel.prototype=new google.maps.OverlayView;MarkerLabel.prototype.onAdd=function(){var pane=this.getPanes().overlayImage.appendChild(this.div);var self=this;this.listeners=[google.maps.event.addListener(this,"position_changed",function(){self.draw()}),google.maps.event.addListener(this,"text_changed",function(){self.draw()}),google.maps.event.addListener(this,"zindex_changed",function(){self.draw()})]};MarkerLabel.prototype.onRemove=function(){this.div.parentNode.removeChild(this.div);for(var i=0,I=this.listeners.length;i<I;++i){google.maps.event.removeListener(this.listeners[i])}};MarkerLabel.prototype.draw=function(){var projection=this.getProjection();var position=projection.fromLatLngToDivPixel(this.get("position"));var div=this.div;this.div.innerHTML=this.get("text").toString();div.style.zIndex=this.get("zIndex");div.style.display="block";div.style.left=position.x-div.offsetWidth/2+"px";div.style.top=position.y-div.offsetHeight+"px"};
function officeAddress() {
    var geocoder = new google.maps.Geocoder(),
        map,
        mapCanvas = document.getElementById('office-map'),
        mapOptions = {
			zoom: 16,
			scrollwheel: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [
				{stylers: [{ visibility: 'simplified' }]},
				{elementType: 'labels', stylers: [{ visibility: 'on' }]}
			]
 	    };
    geocoder.geocode({
        'address': '<?php echo htmlspecialchars(hioice_contact_address_str()); ?>'
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            mapOptions.center = results[0].geometry.location;
            map = new google.maps.Map(mapCanvas, mapOptions);

            var marker = new google.maps.Marker({
			    icon: {
				  path: MAP_PIN,
				  fillColor: '#e14473',
				  fillOpacity: 1,
				  strokeColor: '',
				  strokeWeight: 0
			    },
                map: map,
                map_icon_label: '<span class="map-icon map-icon-point-of-interest"></span>',
                position: mapOptions.center
            });
        }
    });
}
google.maps.event.addDomListener(window, 'load', officeAddress);
</script>
		</div>
	</div>
<?php
	else :
?>
	<div id="contact-cta-banner" class="contact-banner" role="complementary">
		<div id="contact-cta-panel">
			<h3 class="contact-cta-heading">
				<a class="contact-cta-link scf_trigger" href="<?php echo get_permalink(get_theme_mod('maxmedia_contactpage_id')); ?>" title="<?php echo get_theme_mod('maxmedia_contact_cta_title'); ?>"><span><?php echo get_theme_mod('maxmedia_contact_cta_text'); ?></span></a>
			</h3>
			<p class="contact-cta-text"><?php echo get_theme_mod('maxmedia_contact_cta_desc'); ?></p>
		</div>
	</div>
	</div>
<?php
	endif;
?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div class="wrap">
<?php
	if ( is_active_sidebar( 'footer-panel-1' ) ||
		 is_active_sidebar( 'footer-panel-2' ) ) :
?>
				<div class="widget-area" role="complementary">
<?php
		if ( is_active_sidebar( 'footer-panel-1' ) ) : ?>
					<div class="widget-column footer-widget-1">
						<?php dynamic_sidebar( 'footer-panel-1' ); ?>
					</div>
<?php
		endif;
		if ( is_active_sidebar( 'footer-panel-2' ) ) : ?>
					<div class="widget-column footer-widget-2">
						<?php dynamic_sidebar( 'footer-panel-2' ); ?>
					</div>
<?php 	endif; ?>
				</div><!-- .widget-area -->
<?php
	endif; ?>
			</div><!-- .wrap -->
			<?php do_action( 'maxmedia_footer_output' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
