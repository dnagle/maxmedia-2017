
<?php

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

<?php if ( is_active_sidebar( 'front-panel-1' ) ) : ?>
	<div id="front-panel-1" class="front-panel widget-area" role="complementary">
		<?php dynamic_sidebar( 'front-panel-1' ); ?>
	</div><!-- #front-panel-1 -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'front-panel-2' ) ) : ?>
	<div id="front-panel-2" class="front-panel widget-area" role="complementary">
		<?php dynamic_sidebar( 'front-panel-2' ); ?>
	</div><!-- #front-panel-2 -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'front-panel-3' ) ) : ?>
	<div id="front-panel-3" class="front-panel widget-area" role="complementary">
		<!-- Showcase -->
		<?php dynamic_sidebar( 'front-panel-3' ); ?>
	</div><!-- #front-panel-3 -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'front-panel-4' ) ) : ?>
	<div id="front-panel-4" class="front-panel widget-area" role="complementary">
		<?php dynamic_sidebar( 'front-panel-4' ); ?>
	</div><!-- #front-panel-4 -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'front-panel-5' ) ) : ?>
	<div id="front-panel-5" class="front-panel widget-area" role="complementary">
		<?php dynamic_sidebar( 'front-panel-5' ); ?>
	</div><!-- #front-panel-4 -->
<?php endif; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
