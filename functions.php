<?php
/**
 * maxmedia_2017 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package maxmedia_2017
 */

if ( ! function_exists( 'maxmedia_2017_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function maxmedia_2017_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on maxmedia_2017, use a find and replace
		 * to change 'maxmedia-2017' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'maxmedia-2017', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'maxmedia-2017' ),
			'social' => __( 'Social Links Menu', 'maxmedia-2017' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'maxmedia_2017_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'maxmedia_2017_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function maxmedia_2017_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'maxmedia_2017_content_width', 640 );
}
add_action( 'after_setup_theme', 'maxmedia_2017_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function maxmedia_2017_widgets_init() {
	register_sidebar(array(
		'name' => esc_html__( 'Home Panel 1', 'maxmedia-2017' ),
		'id'   => 'front-panel-1',
		'description'   => esc_html__( 'Widget Area 1', 'maxmedia-2017' ),
		'before_widget' => '<div class="front-page-1"><div class="wrap widget-area fadeup-effect">',
		'after_widget'  => maxmedia_front_page_1_widget_area_after(),
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name' => esc_html__( 'Home Panel 2', 'maxmedia-2017' ),
		'id'   => 'front-panel-2',
		'description'   => esc_html__( 'Widget Area 2', 'maxmedia-2017' ),
		'before_widget' => '<div class="front-page-2 wrap widget-area">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name' => esc_html__( 'Home Panel 3', 'maxmedia-2017' ),
		'id'   => 'front-panel-3',
		'description'   => esc_html__( 'Widget Area 3', 'maxmedia-2017' ),
		'before_widget' => '<div class="front-page-3 wrap widget-area">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name' => esc_html__( 'Home Panel 4', 'maxmedia-2017' ),
		'id'   => 'front-panel-4',
		'description'   => esc_html__( 'Widget Area 4', 'maxmedia-2017' ),
		'before_widget' => '<div class="front-page-4 wrap widget-area">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name' => esc_html__( 'Home Panel 5', 'maxmedia-2017' ),
		'id'   => 'front-panel-5',
		'description'   => esc_html__( 'Widget Area 5', 'maxmedia-2017' ),
		'before_widget' => '<div class="front-page-5 wrap widget-area">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));

	register_sidebar(array(
		'name' => esc_html__( 'Footer 1', 'maxmedia-2017' ),
		'id'   => 'footer-panel-1',
		'description'   => esc_html__( 'Footer Widget Area 1', 'maxmedia-2017' ),
		'before_widget' => '<div class="footer-panel-1">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
	));

	register_sidebar(array(
		'name' => esc_html__( 'Footer 2', 'maxmedia-2017' ),
		'id'   => 'footer-panel-2',
		'description'   => esc_html__( 'Footer Widget Area 2', 'maxmedia-2017' ),
		'before_widget' => '<div class="footer-panel-2">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'maxmedia-2017' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'maxmedia-2017' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'maxmedia_2017_widgets_init' );

/**
 * Add responsive background image to front-page-1 widget.
 */
function maxmedia_front_page_1_widget_area_after() {

	require_once get_stylesheet_directory() . '/inc/class-mobile-detect.php';

	$detect = new Handy_Mobile_Detect;

	$image_tag = ''; //sensible default

	$my_theme_slug = get_option('stylesheet');

	$my_theme_mods = get_option("theme_mods_$my_theme_slug");

	$my_background_image = $my_theme_mods['frontpage_background_image'];

	if ( $my_background_image ) {
		$image_src = wp_get_attachment_image_src($my_background_image);
		$image_id = hioice_get_attachment_id_from_url($my_background_image);
		$image_tag = hioice_responsive_thumbnail($image_id, '640w');
	}

	$output = '</div><div id="front-page-1-bg" class="bg-stretch">' . $image_tag . '</div></div>';

	return $output;

}

/**
 * Enqueue scripts and styles.
 */
function maxmedia_2017_scripts() {
	wp_enqueue_style( 'maxmedia-2017-style', get_stylesheet_uri() );

	wp_enqueue_script( 'maxmedia-2017-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'maxmedia-2017-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'maxmedia-2017-theme', get_template_directory_uri() . '/js/theme.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'maxmedia_2017_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


function maxmedia_clean_scripts() {
    global $wp_scripts;
    if (!is_a($wp_scripts, 'WP_Scripts'))
        return;
    foreach ($wp_scripts->registered as $handle => $script)
        $wp_scripts->registered[$handle]->ver = null;
}

function maxmedia_clean_styles() {
    global $wp_styles;
    if (!is_a($wp_styles, 'WP_Styles'))
        return;
    foreach ($wp_styles->registered as $handle => $style)
        $wp_styles->registered[$handle]->ver = null;
}

add_action('wp_print_scripts', 'maxmedia_clean_scripts', 999);
add_action('wp_print_footer_scripts', 'maxmedia_clean_scripts', 999);

add_action('admin_print_styles', 'maxmedia_clean_styles', 999);
add_action('wp_print_styles', 'maxmedia_clean_styles', 999);
