<?php
/**
 * maxmedia_2017 Theme Customizer
 *
 * @package maxmedia_2017
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function maxmedia_2017_customize_register( $wp_customize ) {

	/**
	 * Contact Us Call To Action.
	 */

	$wp_customize->add_section( 'contact_section', array(
		'title'    => __( 'Contact Us Call-To-Action', 'maxmedia' ),
		'priority' => 130, // Before Additional CSS.
	) );

	$wp_customize->add_setting( 'maxmedia_contactpage_id', array(
	  'capability' => 'edit_theme_options',
	  'sanitize_callback' => 'maxmedia_sanitize_dropdown_pages',
	) );

	$wp_customize->add_control( 'maxmedia_contactpage_id', array(
	  'type' => 'dropdown-pages',
	  'section' => 'contact_section',
	  'label' => __( 'Contact Page' ),
	  'description' => __( 'Select the contact page.', 'maxmedia' ),
	) );

	$wp_customize->add_setting( 'maxmedia_contact_cta_text', array(
		'default'           => __( 'Get in touch', 'maxmedia' )
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'maxmedia_contact_cta_text', array(
		'label'      => __( 'Call-To-Action Link Text', 'maxmedia' ),
		'section'    => 'contact_section',
		'settings'   => 'maxmedia_contact_cta_text',
		'type'       => 'text',
	) ) );

	$wp_customize->add_setting( 'maxmedia_contact_cta_title', array(
		'default'           => __( 'Contact us using web form', 'maxmedia' )
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'maxmedia_contact_cta_title', array(
		'label'      => __( 'Call-To-Action Link Title', 'maxmedia' ),
		'description' => __( 'Accessibility text (optional).', 'maxmedia' ),
		'section'    => 'contact_section',
		'settings'   => 'maxmedia_contact_cta_title',
		'type'       => 'text',
	) ) );

	$wp_customize->add_setting( 'maxmedia_contact_cta_desc', array(
		'default'           => ''
	) );

	$wp_customize->add_control( new HIOICE_WP_Textarea_Control(
		$wp_customize, 'maxmedia_contact_cta_desc', array(
		'label'      => __( 'Call-To-Action Content', 'maxmedia' ),
		'section'    => 'contact_section',
		'settings'   => 'maxmedia_contact_cta_desc',
		'type'       => 'textarea',
	) ) );

	/**
	 * Background Image
	 */

	$wp_customize->add_section( 'frontpage_section', array(
		'title'    => __( 'Front Page Options', 'maxmedia' ),
		'priority' => 120, // Before Additional CSS.
	) );

	$wp_customize->add_setting(
		'frontpage_background_image',
		array(
			'default'    => '',
			'capability' => 'manage_options',
			'transport'  => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'frontpage_background_image',
			array(
				'label'    => 'Background Image',
				'section'  => 'frontpage_section',
				'settings' => 'frontpage_background_image',
			)
		)
	);

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'maxmedia_2017_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'maxmedia_2017_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'maxmedia_2017_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function maxmedia_2017_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function maxmedia_2017_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function maxmedia_2017_customize_preview_js() {
	wp_enqueue_script( 'maxmedia-2017-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'maxmedia_2017_customize_preview_js' );

if ( class_exists( 'WP_Customize_Control' ) ) :

	class HIOICE_WP_Textarea_Control extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 *
		 * @access public
		 * @since  1.0
		 * @var    string
		 */
		public $type = 'textarea';

		public function __construct( $manager, $id, $args = array() ) {
			parent::__construct( $manager, $id, $args );
		}

		protected function render_content() {
		?>
			<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<textarea class="large-text" cols="20" rows="5" <?php $this->link(); ?>>
			<?php echo esc_textarea( $this->value() ); ?>
			</textarea>
			</label>
		<?php
		}

	}

endif;


function maxmedia_sanitize_dropdown_pages( $page_id, $setting ) {
	// Ensure $input is an absolute integer.
	$page_id = absint( $page_id );

	// If $page_id is an ID of a published page, return it; otherwise, return the default.
	return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
}

