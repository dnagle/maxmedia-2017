<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package maxmedia_2017
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function maxmedia_2017_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'maxmedia_2017_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function maxmedia_2017_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'maxmedia_2017_pingback_header' );

/**
 * Add a shortcode for displaying hexagon headings.
 */
function maxmedia_2017_hexagon_heading_shortcode( $atts, $content = null ) {

	if ( is_null( $content ) ) {
		return $content;
	}

	$defaults = array(
		'id' => '',
		'class' => '',
		'image' => 'one',
		'width' => '250',
		'svg_url' => '',
		'png_url' => '',
		'heading' => '2'
	);

	extract( shortcode_atts($defaults, $atts) );

	$heading_level = maxmedia_2017_hexagon_heading_set_heading_level($heading);

	if ( empty($svg_url) && empty($png_url) ) {
		$bg_image = maxmedia_2017_hexagon_heading_set_bg_image($image);
		$svg_url = get_template_directory_uri() . '/images/hexagon-' . $bg_image . '.svg';
		$fallback_url = get_template_directory_uri() . '/images/hexagon-' . $bg_image . '.png';
	}
	
	$output = '<div ';
	if ( ! empty($id) ) {
		$output .= 'id="' . $id . '" ';
	}
	$output .= 'class="mm-hh-container">';
	$output .= '<img src="' . esc_url($svg_url) . '"';
	if ( ! empty($fallback_url) ) {
		$output .= ' onerror="this.src=this.dataset.fallback; this.onerror=null;" data-fallback="' . esc_url($fallback_url) . '"';
	}
	$output .= ' class="mm-hh-img"';
	if ( ! empty($width) ) {
		$output .= ' width="' . esc_attr($width) . '"';
	}
	$output .= ' alt="" /><h' . $heading_level . ' class="mm-hh-heading';
	if ( ! empty($class) ) {
		$output .= ' ' . $class;
	}
	$output .= '"><span class="mm-hh-wrap">' . $content . '</span></h' . $heading_level . '></div>';

	return $output;
}

function maxmedia_2017_hexagon_heading_set_heading_level( $level ) {
	// Set a default heading level
	$result = '2';
	if ( is_numeric($level) ) {
		$result = (string) floor($level);
	}
	return $result;
}

function maxmedia_2017_hexagon_heading_set_bg_image( $name ) {
	// Valid image names
	$image_names = array( 'one', 'two' );
	// Set default image name
	$result = $image_names[0];

	$name = strtolower($name);

	if (in_array( $name, $image_names )) {
		$result = $name;
	}

	return $result;
}

/*
 * Register shortcodes in 'init'.
 */
add_action( 'init', 'maxmedia_2017_hexagon_heading_register_shortcodes' );
 
/*
 * Function for registering the [hex-heading] shortcode.
 */
function maxmedia_2017_hexagon_heading_register_shortcodes() {
	add_shortcode( 'hex-heading', 'maxmedia_2017_hexagon_heading_shortcode' );
}

/**
 * Get an attachment ID given a URL.
 * 
 * @param string $url
 *
 * @return int Attachment ID on success, 0 on failure
 */
function hioice_get_attachment_id_from_url( $url ) {

	// default return value
	$attachment_id = 0;

	// get upload directory
	$dir = wp_upload_dir();

	if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) {

		$file = basename( $url );

		$query_args = array(
			'post_type'   => 'attachment',
			'post_status' => 'inherit',
			'fields'      => 'ids',
			'meta_query'  => array(
				array(
					'value'   => $file,
					'compare' => 'LIKE',
					'key'     => '_wp_attachment_metadata',
				),
			)
		);

		$query = new WP_Query( $query_args );

		if ( $query->have_posts() ) {
			foreach ( $query->posts as $post_id ) {
				$meta = wp_get_attachment_metadata( $post_id );
				$original_file = basename( $meta['file'] );
				$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );

				if ( $original_file === $file ||
					in_array( $file, $cropped_image_files ) ) {
					$attachment_id = $post_id;
					break;
				}
			}
		}
	}

	return $attachment_id;

}

// Custom image size support
add_theme_support( 'post-thumbnails' );

add_action( 'after_setup_theme', 'hioice_setup_thumbnails' );

function hioice_default_image_widths() {
	$widths = array('240w', '480w', '640w', '854w', '1024w', '1136w', '1280w', '1366w', '1600w', '1920w', '2048w', '2560w', '3840w', '4096w', '7680w' );
	return $widths;
}

function hioice_setup_thumbnails() {

	$image_sizes = array(
		'16_9' => array(
			'7680w' => array( 7680, 4320 ),
			'4096w' => array( 4096, 2304 ),
			'3840w' => array( 3840, 2160 ),
			'2560w' => array( 2560, 1440 ),
			'2048w' => array( 2048, 1152 ),
			'1920w' => array( 1920, 1080 ),
			'1600w' => array( 1600, 900 ),
			'1366w' => array( 1366, 768 ),
			'1280w' => array( 1280, 720 ),
			'1136w' => array( 1136, 640 ),
			'1024w' => array( 1024, 600 ),
			'854w' => array( 854, 480 ),
			'640w' => array( 640, 360 ),
			'480w' => array( 480, 272 ),
			'240w' => array( 240, 136 )
		),
		'4_3' => array(
			'2048w_43' => array( 2048, 1536 ),
			'1600w_43' => array( 1600, 1200 ),
			'1400w_43' => array( 1400, 1050 ),
			'1280w_43' => array( 1280, 960 ),
			'1024w_43' => array( 1024, 768 ),
			'640w_43' => array( 640, 480 ),
			'480w_43' => array( 480, 360 ),
			'320w_43' => array( 320, 240 ),
			'240w_43' => array( 240, 180 )
		),
		'1_5' => array(
			'600w_15' => array( 600, 400 ),
			'506w_15' => array( 506, 338 ),
			'412w_15' => array( 412, 275 ),
			'375w_15' => array( 375, 250 ),
			'360w_15' => array( 360, 240 ),
			'320w_15' => array( 320, 214 ),
			'240w_15' => array( 240, 160 )
		)
	);

	foreach ($image_sizes as $groups => $sizes) {
		foreach ($sizes as $key => $value) {
			add_image_size( $key, $value[0], $value[1] );
		}
	}

}

function hioice_thumbnail_srcset( $srcset_arr ) {

	$srcset = '';

	foreach($srcset_arr as $key => $value) {
		if ($srcset) {
			$srcset .= ', ';
		}
		$srcset .= $value . ' ' . $key;
	}

	return $srcset;

}

function hioice_responsive_thumbnail( $image_id, $size = 'image', $class = '' ) {

	$thumbnail = wp_get_attachment_image_src( $image_id, $size )[0];

	$thumbnail_sizes = hioice_default_image_widths();

	$thumbnail_metadata = wp_get_attachment_metadata( $image_id );

	$srcset = array();

	foreach ($thumbnail_sizes as $size) {
		if ( array_key_exists($size, $thumbnail_metadata['sizes']) ) {
			$srcset[$size] = wp_get_attachment_image_src( $image_id, $size )[0];
		}
	}

	$img_size = $thumbnail_metadata['width'] . "w";
	$srcset[$img_size] = wp_get_attachment_image_src( $image_id, $img_size )[0];

	$img_tag  = '<img src="' . $thumbnail . '"';
	$img_tag .= ( $srcset ? ' srcset="' . hioice_thumbnail_srcset($srcset) . '"': '' );
	$img_tag .= ( $class ? ' class="' . esc_attr($class) . '"' : '' );
	$img_tag .= ' sizes="auto">';

	return $img_tag;

}

add_action('maxmedia_footer_output', 'maxmedia_display_legal_footer');
function maxmedia_display_legal_footer() {
	if ( shortcode_exists( 'hioice_legal_footer' ) ) {
		do_shortcode('[hioice_legal_footer]');
	}
}

/**
 * Display SVG icons in social links menu.
 *
 * @param  string  $item_output The menu item output.
 * @param  WP_Post $item        Menu item object.
 * @param  int     $depth       Depth of the menu.
 * @param  array   $args        wp_nav_menu() arguments.
 * @return string  $item_output The menu item output with social icon.
 */
function maxmedia_nav_menu_social_icons( $item_output, $item, $depth, $args ) {
	// Get supported social icons.
	$social_icons = maxmedia_social_links_icons();

	// Change SVG icon inside social links menu if there is supported URL.
	if ( 'social' === $args->theme_location ) {
		foreach ( $social_icons as $attr => $value ) {
			if ( false !== strpos( $item_output, $attr ) ) {
				$item_output = str_replace( $args->link_after, '</span>' . maxmedia_get_svg( array( 'icon' => esc_attr( $value ) ) ), $item_output );
			}
		}
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'maxmedia_nav_menu_social_icons', 10, 4 );

/**
 * Returns an array of supported social links (URL and icon name).
 *
 * @return array $social_links_icons
 */
function maxmedia_social_links_icons() {
	// Supported social links icons.
	$social_links_icons = array(
		'facebook.com'    => 'facebook-1',
		'plus.google.com' => 'gplus-1',
		'linkedin.com'    => 'linkedin-1',
		'mailto:'         => 'mail-1',
		'twitter.com'     => 'twitter-1',
	);

	/**
	 * Filter social links icons.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param array $social_links_icons Array of social links icons.
	 */
	return apply_filters( 'maxmedia_social_links_icons', $social_links_icons );
}

function maxmedia_get_svg( $args = array() ) {
	// Make sure $args are an array.
	if ( empty( $args ) ) {
		return __( 'Please define default parameters in the form of an array.', 'twentyseventeen' );
	}

	// Define an icon.
	if ( false === array_key_exists( 'icon', $args ) ) {
		return __( 'Please define an SVG icon filename.', 'twentyseventeen' );
	}

	// Set defaults.
	$defaults = array(
		'icon'        => '',
		'title'       => '',
		'desc'        => '',
		'fallback'    => false,
	);

	// Parse args.
	$args = wp_parse_args( $args, $defaults );

	// Set aria hidden.
	$aria_hidden = ' aria-hidden="true"';

	// Set ARIA.
	$aria_labelledby = '';

	/*
	 * Twenty Seventeen doesn't use the SVG title or description attributes; non-decorative icons are described with .screen-reader-text.
	 *
	 * However, child themes can use the title and description to add information to non-decorative SVG icons to improve accessibility.
	 *
	 * Example 1 with title: <?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right', 'title' => __( 'This is the title', 'textdomain' ) ) ); ?>
	 *
	 * Example 2 with title and description: <?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right', 'title' => __( 'This is the title', 'textdomain' ), 'desc' => __( 'This is the description', 'textdomain' ) ) ); ?>
	 *
	 * See https://www.paciellogroup.com/blog/2013/12/using-aria-enhance-svg-accessibility/.
	 */
	if ( $args['title'] ) {
		$aria_hidden     = '';
		$unique_id       = uniqid();
		$aria_labelledby = ' aria-labelledby="title-' . $unique_id . '"';

		if ( $args['desc'] ) {
			$aria_labelledby = ' aria-labelledby="title-' . $unique_id . ' desc-' . $unique_id . '"';
		}
	}
/*
https://codepen.io/Onomicon/pen/iDfld
<div class="icon-wrapper"><i class="fa fa-users custom-icon"><span class="fix-editor">&nbsp;</span></i></div>
*/
	// Begin SVG markup.
	$svg = '<div class="icon-wrapper"><i class="icon icon-' . esc_attr( $args['icon'] ) . ' custom-icon"' . $aria_hidden . $aria_labelledby . ' role="img">';

	// Display the title.
	if ( $args['title'] ) {
		$svg .= '<title id="title-' . $unique_id . '">' . esc_html( $args['title'] ) . '</title>';

		// Display the desc only if the title is already set.
		if ( $args['desc'] ) {
			$svg .= '<desc id="desc-' . $unique_id . '">' . esc_html( $args['desc'] ) . '</desc>';
		}
	}

	/*
	 * Display the icon.
	 *
	 * The whitespace around `<use>` is intentional - it is a work around to a keyboard navigation bug in Safari 10.
	 *
	 * See https://core.trac.wordpress.org/ticket/38387.
	 */
	$svg .= ' <use href="#icon-' . esc_html( $args['icon'] ) . '" xlink:href="#icon-' . esc_html( $args['icon'] ) . '"></use> ';

	// Add some markup to use as a fallback for browsers that do not support SVGs.
	if ( $args['fallback'] ) {
		$svg .= '<span class="svg-fallback icon-' . esc_attr( $args['icon'] ) . '"></span>';
	}

	$svg .= '<span class="fix-editor">&nbsp;</span></i></div>';

	return $svg;
}
