/*!
 * enquire.js v2.1.6 - Awesome Media Queries in JavaScript
 * Copyright (c) 2017 Nick Williams - http://wicky.nillia.ms/enquire.js
 * License: MIT */
! function(a) { if ("object" == typeof exports && "undefined" != typeof module) module.exports = a();
    else if ("function" == typeof define && define.amd) define([], a);
    else { var b;
        b = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, b.enquire = a() } }(function() { return function a(b, c, d) {
        function e(g, h) { if (!c[g]) { if (!b[g]) { var i = "function" == typeof require && require; if (!h && i) return i(g, !0); if (f) return f(g, !0); var j = new Error("Cannot find module '" + g + "'"); throw j.code = "MODULE_NOT_FOUND", j } var k = c[g] = { exports: {} };
                b[g][0].call(k.exports, function(a) { var c = b[g][1][a]; return e(c ? c : a) }, k, k.exports, a, b, c, d) } return c[g].exports } for (var f = "function" == typeof require && require, g = 0; g < d.length; g++) e(d[g]); return e }({ 1: [function(a, b, c) {
            function d(a, b) { this.query = a, this.isUnconditional = b, this.handlers = [], this.mql = window.matchMedia(a); var c = this;
                this.listener = function(a) { c.mql = a.currentTarget || a, c.assess() }, this.mql.addListener(this.listener) } var e = a(3),
                f = a(4).each;
            d.prototype = { constuctor: d, addHandler: function(a) { var b = new e(a);
                    this.handlers.push(b), this.matches() && b.on() }, removeHandler: function(a) { var b = this.handlers;
                    f(b, function(c, d) { if (c.equals(a)) return c.destroy(), !b.splice(d, 1) }) }, matches: function() { return this.mql.matches || this.isUnconditional }, clear: function() { f(this.handlers, function(a) { a.destroy() }), this.mql.removeListener(this.listener), this.handlers.length = 0 }, assess: function() { var a = this.matches() ? "on" : "off";
                    f(this.handlers, function(b) { b[a]() }) } }, b.exports = d }, { 3: 3, 4: 4 }], 2: [function(a, b, c) {
            function d() { if (!window.matchMedia) throw new Error("matchMedia not present, legacy browsers require a polyfill");
                this.queries = {}, this.browserIsIncapable = !window.matchMedia("only all").matches } var e = a(1),
                f = a(4),
                g = f.each,
                h = f.isFunction,
                i = f.isArray;
            d.prototype = { constructor: d, register: function(a, b, c) { var d = this.queries,
                        f = c && this.browserIsIncapable; return d[a] || (d[a] = new e(a, f)), h(b) && (b = { match: b }), i(b) || (b = [b]), g(b, function(b) { h(b) && (b = { match: b }), d[a].addHandler(b) }), this }, unregister: function(a, b) { var c = this.queries[a]; return c && (b ? c.removeHandler(b) : (c.clear(), delete this.queries[a])), this } }, b.exports = d }, { 1: 1, 4: 4 }], 3: [function(a, b, c) {
            function d(a) { this.options = a, !a.deferSetup && this.setup() } d.prototype = { constructor: d, setup: function() { this.options.setup && this.options.setup(), this.initialised = !0 }, on: function() {!this.initialised && this.setup(), this.options.match && this.options.match() }, off: function() { this.options.unmatch && this.options.unmatch() }, destroy: function() { this.options.destroy ? this.options.destroy() : this.off() }, equals: function(a) { return this.options === a || this.options.match === a } }, b.exports = d }, {}], 4: [function(a, b, c) {
            function d(a, b) { var c = 0,
                    d = a.length; for (c; c < d && b(a[c], c) !== !1; c++); }

            function e(a) { return "[object Array]" === Object.prototype.toString.apply(a) }

            function f(a) { return "function" == typeof a } b.exports = { isFunction: f, isArray: e, each: d } }, {}], 5: [function(a, b, c) { var d = a(2);
            b.exports = new d }, { 2: 2 }] }, {}, [5])(5) });
/* onPageLoad */
(function(document, undefined) {

    'use strict';

    var showcaseState = showcaseState || {};

    var addClass, hasClass, nextSlide, prevSlide, removeClass, slideAction;

    hasClass = function(el, className) {
        return (el.classList) ? el.classList.contains(className) : new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
    };

    addClass = function(el, className) {
        if (el.classList) {
            el.classList.add(className);
        } else {
            el.className += ' ' + className;
        }
    };

    removeClass = function(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    };

    function has_svg_support() {
        return document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Image', '1.1');
    }

    if (!has_svg_support) {
        var svg_images = document.querySelectorAll('img[src$=".svg"]');
    }

    function isMobileUserAgent() {
        // Code based on http://detectmobilebrowsers.com/
        var isMobileUA = false,
            ua = navigator.userAgent || navigator.vendor || window.opera;
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(ua) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(ua.substr(0, 4))) {
            isMobileUA = true;
        }
        return isMobileUA;
    }

    function isMobileDevice() {
        //    return (typeof window.orientation !== 'undefined') || (navigator.userAgent.indexOf('IEMobile') !== -1) || isMobileUserAgent();
        return isMobileUserAgent();
    }

    /**
     * Callback to fix skip links focus.
     *
     * @since 2.2.0
     */
    function ga_skiplinks() {

        var elem = document.getElementById(location.hash.substring(1));

        if (elem) {
            if (!/^(?:a|select|input|button|textarea)$/i.test(elem.tagName)) {
                elem.tabIndex = -1;
            }
            elem.focus();
        }
    }

    if (window.addEventListener) {
        window.addEventListener('hashchange', ga_skiplinks, false);
    } else { // IE8 and earlier.
        window.attachEvent('onhashchange', ga_skiplinks, false);
    }

    // Reference: http://www.html5rocks.com/en/tutorials/speed/animations/

    var last_known_scroll_position = 0, ticking = false;

    function doSomething(scroll_pos) {
        //        var scrollTop = ( window.pageYOffset !== undefined ) ? window.pageYOffset : ( document.documentElement || document.body.parentNode || document.body ).scrollTop;
        // do something with the scroll position
        if (scroll_pos > 50) {
            document.querySelector('.site-header').classList.add('shrink');
        } else {
            document.querySelector('.site-header').classList.remove('shrink');
        }

    }

    window.addEventListener('scroll', function(e) {
        last_known_scroll_position = window.scrollY;
        if (!ticking) {
            window.requestAnimationFrame(function() {
                doSomething(last_known_scroll_position);
                ticking = false;
            });
        }
        ticking = true;
    });

    function isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }

    function addClass(elem, className) {
        if (elem.classList)
            elem.classList.add(className);
        else
            elem.className += ' ' + className;
    }

    function hasClass(elem, className) {

        return (elem.classList) ? elem.classList.contains(className) : new RegExp('(^| )' + className + '( |$)', 'gi').test(elem.className);

    }

    function toggleClass(elem, className) {
        if (elem.classList) {
            elem.classList.toggle(className);
        } else {
            var classArr = elem.className.split(' '),
                existingIndex = classArr.indexOf(className);

            if (existingIndex >= 0)
                classArr.splice(existingIndex, 1);
            else
                classArr.push(className);

            elem.className = classArr.join(' ');
        }
    }

    function removeClass(elem, className) {
        if (elem.classList)
            elem.classList.remove(className);
        else
            elem.className = elem.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    /**
     * Calculates the outer height for the given DOM element, including the 
     * contributions of padding, border, and optionally margin.
     * 
     * Replicates http://api.jquery.com/outerheight/
     * 
     * @param el - the element of which to calculate the outer height
     * 
     * @param includeMargins - A Boolean indicating whether to include the element's margin in the calculation
     */
    function calculateOuterHeight(elem, includeMargins) {

        var height = 0,
            attributeHeight = 0,
            attributes = [
                'height',
                'border-top-width',
                'border-bottom-width',
                'padding-top',
                'padding-bottom'
            ];

        if (typeof(includeMargins) !== 'undefined' && includeMargins) {
            attributes = attributes.concat(['margin-top', 'margin-bottom']);
        }

        for (var i = 0; i < attributes.length; i++) {
            attributeHeight = parseInt(window.getComputedStyle(elem, attributes[i]), 10);
            if (!isNaN(attributeHeight)) {
                height += attributeHeight;
            }

        }

        return isNaN(height) ? 0 : height;
    }

    // Keep site header below the header section
    function handleResize() {
        var navBar = document.body.querySelector('.menu-primary'),
            secondary = document.body.querySelector('.menu-secondary');
        if (!isEmpty(navBar)) {
            var navBarHeight = calculateOuterHeight(navBar, false);
            var secondaryHeight = calculateOuterHeight(secondary, false);
            document.querySelector('.site-header').style.paddingTop = navbarHeight + secondaryHeight;
            document.querySelector('.menu-secondary').style.top = navBarHeight;
        }
    }

    function createMenuToggleButton(btnClass, isTopMenu) {
        var newBtn = document.createElement('button');
        newBtn.className = (btnClass.length > 0) ? btnClass : '';
        newBtn.setAttribute('role', 'button');
        if (typeof(isTopMenu) !== 'undefined' && isTopMenu) {
            newBtn.innerHTML = 'Menu';
        }
        return newBtn;
    }

    function removeAnimations() {

        var elements = document.querySelectorAll('.fadeup-effect');

        if (elements.length === 0) {
            return;
        }

        [].slice.call(elements).forEach(function(elem, ix) {
            removeClass(elem, 'fadeup-effect');
            removeClass(elem, 'fadeInUp');
        });

    }

    function initialisePage() {
        // Make sure JS class is added.
        addClass(document.documentElement, 'js');

        addClass(document.querySelector('.site-header'), 'responsive');

        if (isMobileDevice()) {
            removeAnimations();
        }

        /* Responsive Menu
        ------------------------ */
        window.addEventListener('resize', function() {
            handleResize();
        });

        handleResize();

        // Add menu-toggle button to the active menus
        /* https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore */

        // Primary navigation menu
        var navPrimary = document.querySelector('.nav-primary');
        var primaryToggleBtn = createMenuToggleButton('menu-toggle toggle-primary icon-menu', true);
        navPrimary.parentNode.insertBefore(primaryToggleBtn, navPrimary);
        addClass(navPrimary, 'genesis-responsive-menu');

        // Secondary navigation
        var navSecondary = document.querySelector('.nav-secondary .wrap');
        if (typeof(navSecondary) !== 'undefined' && navSecondary && !isEmpty(navSecondary)) {
            var secondaryToggleBtn = createMenuToggleButton('menu-toggle toggle-secondary', true);
            navSecondary.parentNode.insertBefore(secondaryToggleBtn, navSecondary);
        }

        // Add sub-menu-toggle buttons
        document.querySelectorAll('.menu-item-has-children').forEach(function(el, i) {
            var subMenuToggleBtn = createMenuToggleButton('sub-menu-toggle icon-down-open', false);
            el.appendChild(subMenuToggleBtn);
        });

        // Add aria labels
        [].slice.call(document.querySelectorAll('.menu-toggle, .sub-menu-toggle')).forEach(function(elem, ix) {
            elem.setAttribute('aria-expanded', false);
            elem.setAttribute('aria-pressed', false);
        });

        // Smaller screens responsive menu
        document.querySelector('body').addEventListener('click', function(event) {

            var toggleMenu = null,
                target = event.target;

            if (event.target.outerHTML === '<span></span>') {
                target = event.target.parentNode;
            }

            var clickedBtn = (hasClass(target, 'toggle-primary') ? 'primary' : (hasClass(target, 'toggle-secondary') ? 'secondary' : (hasClass(target, 'sub-menu-toggle') ? 'sub-menu' : null)));

            if ((clickedBtn !== null) && (target.tagName.toLowerCase() === 'button')) {
                // Toggle activated class on clicked button
                toggleClass(target, 'activated');
                // Toggle menu display
                switch (clickedBtn) {
                    case 'primary':
                        toggleMenu = target.parentNode.parentNode.querySelector('.nav-primary');
                        break;
                    case 'secondary':
                        toggleMenu = target.parentNode.parentNode.querySelector('.nav-secondary .wrap');
                        break;
                    default:
                        // find a sibling node with clickBtn class name
                        toggleMenu = target.parentNode.querySelector('.' + clickedBtn);
                }
                toggleMenu.style.display = (toggleMenu.style.display === 'block') ? '' : 'block';
                // Toggle aria attributes
                target.setAttribute('aria-expanded', target.getAttribute('aria-expanded') === 'true' ? 'false' : 'true');
                target.setAttribute('aria-pressed', target.getAttribute('aria-pressed') === 'true' ? 'false' : 'true');
            }
        });
    }

    function addPaddingElement(container) {
        if (!container) {
            return;
        }
        // create a new div element 
        // and give it some content 
        var newDiv = document.createElement('div');
        newDiv.style.display = 'none';
        newDiv.classList = 'showcase-panel is-padding';
        newDiv.appendChild(document.createTextNode('&nbsp;'));
        // add the newly created element into the DOM
        container.appendChild(newDiv);
    }

    function initialiseShowcasePadding() {
        // Find the gallery container
        var gallery = showcaseState.galleryElem,
            numPanels = showcaseState.panelCount,
            paddingPanels = gallery.querySelectorAll('.showcase-panel.is-padding'),
            paddingPanelsNeeded = 0,
            paddingAdded = 0,
            clonesRequired,
            documentFragment;

        showcaseState.settingsArray.forEach(function(elem, index) {
            if (elem.padding > paddingPanelsNeeded) {
                paddingPanelsNeeded = elem.padding;
            }
        });

        clonesRequired = paddingPanelsNeeded - paddingPanels.length;

        // Determine whether additional padding elements are required
        if (clonesRequired < 1) {
            return;
        }

        // Clone padding elements
        while (paddingAdded < clonesRequired) {
            documentFragment = paddingPanels[0].cloneNode(true);
            gallery.appendChild(documentFragment);
            paddingAdded++;
        }
    }

    function addMatchMediaListeners() {

        if (enquire.browserIsIncapable) {
            console.log('incapable');
        }

        showcaseState.settingsArray.forEach(function(elem, index) {
            window.enquire.register(elem.mediaQuery, { match: mqWidthChangeHandler });
        });

    }

    function setPaddingItemsVisibility() {
        var paddingItems = showcaseState.galleryElem.querySelectorAll('.showcase-panel.is-padding');
        paddingItems.forEach(function(elem, index) {
            elem.style.display = (index < showcaseState.paddingItems) ? 'block' : 'none';
        });
    }
    /* mediaQueries */
    var mqWidthChangeHandler = function() {

        var gallery = showcaseState.galleryElem;

        if (!gallery) {
            return;
        }

        if (window.matchMedia(showcaseState.settingsArray[0].mediaQuery).matches) {
            // single-column tweaks
            gallery.style.margin = 0;
        } else {
            gallery.style.margin = '';
        }

        showcaseState.settingsArray.forEach(function(elem, index) {
            if (window.matchMedia(elem.mediaQuery).matches) {
                showcaseState.galleryColumns = elem.columns;
                showcaseState.paddingItems = elem.padding;
            }
        });

        setPaddingItemsVisibility();
    }
    /* on page stuff */
    var optimizedResize = (function() {

        var callbacks = [],
            running = false;

        // fired on resize event
        function resize() {
            if (!running) {
                running = true;

                if (window.requestAnimationFrame) {
                    window.requestAnimationFrame(runCallbacks);
                } else {
                    setTimeout(runCallbacks, 66);
                }
            }
        }

        // run the actual callbacks
        function runCallbacks() {
            callbacks.forEach(function(callback) {
                callback();
            });
            running = false;
        }

        // adds callback to loop
        function addCallback(callback) {
            if (callback) {
                callbacks.push(callback);
            }
        }

        return {
            // public method to add additional callback
            add: function(callback) {
                if (!callbacks.length) {
                    window.addEventListener('resize', resize);
                }
                addCallback(callback);
            }
        }
    }());

    // setup the state model
    function initialiseShowcaseModel() {
        showcaseState.rootElem = document.querySelector('#front-panel-3 > div');
        showcaseState.galleryElem = showcaseState.rootElem.querySelector('.showcase-grid.showcase-gallery');
        showcaseState.panelCount = showcaseState.galleryElem.dataset.panels - 0;
        if (typeof(showcaseState.panelCount) === 'undefined' || !showcaseState.panelCount) {
            showcaseState.panelCount = showcaseState.galleryElem.querySelectorAll('.showcase-panel.is-panel').length;
        }
        // media queries - need to match the CSS
        showcaseState.settingsArray = [
            { mediaQuery: '(max-width: 600px)', columns: 1, padding: 0 },
            { mediaQuery: '(min-width: 601px) and (max-width: 900px)', columns: 2 },
            { mediaQuery: '(min-width: 901px) and (max-width: 1200px)', columns: 3 },
            { mediaQuery: '(min-width: 1201px)', columns: 4 }
        ];
        // initialise galleryColumns
        showcaseState.settingsArray.forEach(function(elem, index) {
            elem.padding = (showcaseState.panelCount % elem.columns > 0) ? elem.columns - (showcaseState.panelCount % elem.columns) : 0;
            if (window.matchMedia(elem.mediaQuery).matches) {
                showcaseState.galleryColumns = elem.columns;
                showcaseState.paddingItems = elem.padding;
            }
        });
    }
    // Initialise the showcase elements
    document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
            if (hasClass(document.body, 'home')) {
                // Home page specific tweaks
                initialiseShowcaseModel();
                initialiseShowcasePadding();
                addMatchMediaListeners();
                // start resize monitoring process
                //optimizedResize.add(function() {
                //});
            }
        }
    });

    nextSlide = function(el) {
        if (el.nextElementSibling) {
            return el.nextElementSibling;
        } else {
            return el.parentNode.children[0];
        }
    };

    prevSlide = function(el) {
        if (el.previousElementSibling) {
            return el.previousElementSibling;
        } else {
            return el.parentNode.children[el.parentNode.childElementCount - 1];
        }
    };

    slideAction = function(carousel, direction) {

        var asc, el, end, i, slide, slides;

        slides = carousel.querySelectorAll('.carousel-slide');

        el = carousel.querySelector('.reference');

        removeClass(el, 'reference');

        if (direction === 'next') {
            slide = nextSlide(el);
            removeClass(carousel, 'go-prev');
        } else {
            slide = prevSlide(el);
            addClass(carousel, 'go-prev');
        }

        addClass(slide, 'reference');

        slide.style.order = 1;

        for (i = 2, end = slides.length, asc = 2 <= end; asc ? i <= end : i >= end; asc ? i++ : i--) {
            slide = nextSlide(slide);
            slide.style.order = i;
        }

        removeClass(carousel, 'has-transformed');

        return setTimeout(function() {
            addClass(carousel, 'has-transformed');
        }, 100);

    };

    document.addEventListener('click', function(e) {
        var carousel, direction;

        if (hasClass(e.target, 'carousel-btn')) {
            carousel = e.target.parentNode.parentNode.querySelector('.carousel');
            direction = (hasClass(e.target, 'carousel-next')) ? 'next' : 'prev';
            slideAction(carousel, direction);
        }

    });

})(document);