<?php
/**
 * Template Name: Our Team Page
 *
 * The template for displaying Our Team page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package maxmedia_2017
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
		?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="our-story-container">
				<?php echo maxmedia_2017_hexagon_heading_shortcode( array( 'image' => 'one' ), get_the_title() ); ?>
				<?php the_content(); ?>
			</div><!-- .contact-form-container -->

			</article><!-- #post-<?php the_ID(); ?> -->

		<?php
		endwhile; // End of the loop.

		if ( shortcode_exists( 'hioice-our-team' ) ) :
		?>
			<div id="our-team">
		<?php 
			echo maxmedia_2017_hexagon_heading_shortcode( array( 'image' => 'two' ), 'Our Team' );

			echo do_shortcode('[hioice-our-team]');
		?>
			</div>
		<?php
		endif;
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
